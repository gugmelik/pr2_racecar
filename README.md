# Smart Car Simulation
Code Usage：run every command in seperate terminal
```bash
roslaunch racecar_gazebo racecar_runway_navigation.launch
roslaunch racecar_gazebo racecar_rviz.launch
rosrun racecar_gazebo path_pursuit.py  
```

# Report 

[Video report](https://drive.google.com/file/d/18_Z_BTTabokOCAPIIB9tRaqFqcwlEctx/view?usp=share_link)

# bug summary

## Error controllers related
```bash
sudo apt-get install ros-noetic-controller-manager
sudo apt-get install ros-noetic-gazebo-ros-control
sudo apt-get install ros-noetic-effort-controllers
sudo apt-get install ros-noetic-joint-state-controller
```

## Error driver_base related
```bash
sudo apt-get install ros-noetic-driver-base
```

## Error rtabmap related
```bash
sudo apt-get install ros-noetic-rtabmap-ros
```

## Error ackermann_msgs related

```bash
sudo apt-get install ros-noetic-ackermann-msgs
```

## Error findline.cpp cannot find the opencv header file
Execute: `locate OpenCVConfig.cmake` to get the path of your opencv

Execute: `gedit ~/racecar_ws/src/racecar_gazebo/CMakeLists.txt`

Modify the path on line 7 to your path: set(OpenCV_DIR /opt/ros/kinetic/share/OpenCV-3.3.1-dev/)

## Failed to create the teb_local_planner/TebLocalPlannerROS planner
```bash
sudo apt-get install ros-noetic-teb-local-planner
```

